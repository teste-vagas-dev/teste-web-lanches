using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using TesteWebLanches.Domain.Entities;
using TesteWebLanches.Services.Models;

namespace TesteWebLanches.Services
{
    public class OrderService : IOrderService
    {
        private readonly IRepository<Ingredient> _ingredientRepository;

        public OrderService(IRepository<Ingredient> ingredientRepository)
        {
            _ingredientRepository = ingredientRepository;
        }

        // Calculate the order base on each ingredient
        public async Task<OrderSummaryModel> CalculateOrder(IngredientsToCalculate ingredientsToCalculate)
        {
            var ingredientsIds = ingredientsToCalculate.Ingredients.Select(p => p.IngredientId).ToList();
            var ingredientsDb = await _ingredientRepository.Query()
                .Where(p => ingredientsIds.Contains(p.Id))
                .ToListAsync();

            var orderSummary = new OrderSummaryModel();
            foreach (var ingredient in ingredientsToCalculate.Ingredients.Where(p => p.Quantity > 0))
            {
                var ingredientFromDb = ingredientsDb.FirstOrDefault(p => p.Id == ingredient.IngredientId);
                if (ingredientFromDb == null)
                {
                    throw new DataException($"Was not possible to find the ingredient id: {ingredient.IngredientId}");
                }
                
                orderSummary.OrderItems.Add(new OrderItemSummaryModel
                {
                    IngredientId = ingredientFromDb.Id,
                    IngredientName = ingredientFromDb.Name,
                    Quantity = ingredient.Quantity,
                    PriceUnit = ingredientFromDb.Price,
                    PriceAmount = ingredientFromDb.Price * ingredient.Quantity,
                });

                orderSummary.Amount = orderSummary.OrderItems.Sum(p => p.PriceAmount);
                orderSummary.ItemsNumber = orderSummary.OrderItems.Count;
            }
            
            CheckAndApplyDiscounts(orderSummary);
            
            return orderSummary;
        }

        // Apply all available discount the in order summary
        private void CheckAndApplyDiscounts(OrderSummaryModel orderSummaryModel)
        {
            var orderItems = orderSummaryModel.OrderItems;
            
            const int baconIngredientId = 2;
            const int lettuceIngredientId = 1;
            const int beefIngredientId = 3;
            const int cheeseIngredientId = 5;

            // Check for lettuce without bacon discount
            if (orderItems.Any(p => p.IngredientId == lettuceIngredientId) &&
                !orderItems.Any(p => p.IngredientId == baconIngredientId))
            {
                var percentage = 10;
                var amountDiscount = orderSummaryModel.Amount * percentage / 100;
                orderSummaryModel.DiscountItemsApplied.Add(new OrderDiscountItemModel
                {
                    Amount = amountDiscount,
                    Percentage = percentage,
                    Name = "Light"
                });

                orderSummaryModel.DiscountAmount += amountDiscount;
            }
            
            // Check for each 3 beefs discount
            var beefOrderItemsQuantity = orderItems
                .Where(p => p.IngredientId == beefIngredientId)
                .Sum(p => p.Quantity);
            
            if (beefOrderItemsQuantity > 0)
            {
                var resultDivision = beefOrderItemsQuantity / 3;
                if (resultDivision > 0)
                {
                    var item = orderItems.FirstOrDefault(p => p.IngredientId == beefIngredientId);
                    var amountDiscount = item.PriceUnit * resultDivision;
                    orderSummaryModel.DiscountItemsApplied.Add(new OrderDiscountItemModel
                    {
                        Amount = item.PriceUnit * resultDivision,
                        Name = "Muito carne"
                    });
                    
                    orderSummaryModel.DiscountAmount += amountDiscount;
                }
            }
            
            // Check for each 3 cheeses discount
            var cheeseOrderItemsQuantity = orderItems
                .Where(p => p.IngredientId == cheeseIngredientId)
                .Sum(p => p.Quantity);
            
            if (cheeseOrderItemsQuantity > 0)
            {
                var resultDivision = cheeseOrderItemsQuantity / 3;
                if (resultDivision > 0)
                {
                    var item = orderItems.FirstOrDefault(p => p.IngredientId == cheeseIngredientId);
                    var amountDiscount = item.PriceUnit * resultDivision;
                    orderSummaryModel.DiscountItemsApplied.Add(new OrderDiscountItemModel
                    {
                        Amount = item.PriceUnit * resultDivision,
                        Name = "Muito queijo"
                    });
                    
                    orderSummaryModel.DiscountAmount += amountDiscount;
                }
            }
        }
    }
}