using System.Collections.Generic;
using System.Threading.Tasks;
using TesteWebLanches.Services.Models;

namespace TesteWebLanches.Services
{
    public interface IRecipeService
    {
        Task<List<RecipeToOrderModel>> GetAllRecipesToOrderAsync();
        Task<IngredientRecipeModel> GetNewRecipeBaseByCreatedRecipeAsync(int recipeId);
    }
}