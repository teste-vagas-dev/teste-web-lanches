using System.Collections.Generic;

namespace TesteWebLanches.Services.Models
{
    public class IngredientRecipeModel
    {
        public string RecipeName { get; set; }
        public IList<IngredientRecipeItemModel> IngredientRecipeItems { get; set; }

        public IngredientRecipeModel()
        {
            IngredientRecipeItems = new List<IngredientRecipeItemModel>();
        }
    }
}