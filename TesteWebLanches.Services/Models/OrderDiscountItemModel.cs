namespace TesteWebLanches.Services.Models
{
    public class OrderDiscountItemModel
    {
        public string Name { get; set; }
        public decimal Amount { get; set; }
        public float Percentage { get; set; }
    }
}