using System.Collections.Generic;

namespace TesteWebLanches.Services.Models
{
    public class IngredientsToCalculate
    {
        public List<IngredientRecipeItemModel> Ingredients { get; set; }

        public IngredientsToCalculate()
        {
            Ingredients = new List<IngredientRecipeItemModel>();
        }
    }
}