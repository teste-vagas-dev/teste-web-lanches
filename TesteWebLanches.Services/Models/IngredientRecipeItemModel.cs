namespace TesteWebLanches.Services.Models
{
    public class IngredientRecipeItemModel
    {
        public string Name { get; set; }
        public int IngredientId { get; set; }
        public int Quantity { get; set; }
        public decimal Price { get; set; }
    }
}