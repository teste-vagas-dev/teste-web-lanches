namespace TesteWebLanches.Services.Models
{
    public class IngredientItemModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public decimal Price { get; set; }
        
    }
}