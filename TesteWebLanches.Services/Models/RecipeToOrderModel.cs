using System.Collections.Generic;

namespace TesteWebLanches.Services.Models
{
    public class RecipeToOrderModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int IngredientsNumber { get; set; }
        public decimal Price { get; set; }
        public IList<IngredientItemModel> RecipeIngredientItems { get; set; }

        public RecipeToOrderModel()
        {
            RecipeIngredientItems = new List<IngredientItemModel>();
        }
    }
}