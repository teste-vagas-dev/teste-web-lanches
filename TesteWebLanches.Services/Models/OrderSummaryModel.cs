using System.Collections.Generic;

namespace TesteWebLanches.Services.Models
{
    public class OrderSummaryModel
    {
        public decimal Amount { get; set; }
        public decimal DiscountAmount { get; set; }
        public int ItemsNumber { get; set; }
        public IList<OrderItemSummaryModel> OrderItems { get; set; }
        public IList<OrderDiscountItemModel> DiscountItemsApplied { get; set; }

        public OrderSummaryModel()
        {
            OrderItems = new List<OrderItemSummaryModel>();
            DiscountItemsApplied = new List<OrderDiscountItemModel>();
        }
    }
}