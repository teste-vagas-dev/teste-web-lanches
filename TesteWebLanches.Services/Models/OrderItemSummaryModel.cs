namespace TesteWebLanches.Services.Models
{
    public class OrderItemSummaryModel
    {
        public int IngredientId { get; set; }
        public string IngredientName { get; set; }
        public decimal PriceUnit { get; set; }
        public decimal PriceAmount { get; set; }
        public int Quantity { get; set; }
    }
}