using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using TesteWebLanches.Domain.Entities;
using TesteWebLanches.Services.Models;

namespace TesteWebLanches.Services
{
    public class IngredientService : IIngredientService
    {
        private readonly IRepository<Ingredient> _ingredientRepository;

        public IngredientService(IRepository<Ingredient> ingredientRepository)
        {
            _ingredientRepository = ingredientRepository;
        }

        // Get all available ingredients in the store
        public async Task<List<IngredientItemModel>> GetAvailableIngredientsAsync()
        {
            var ingredients = await  _ingredientRepository.Query().AsNoTracking().ToListAsync();
            if (!ingredients.Any())
            {
                return null;
            }

            var result = ingredients.Select(ingredient => new IngredientItemModel
            {
                Id = ingredient.Id, 
                Name = ingredient.Name,
                Price = ingredient.Price
            }).ToList();

            return result;
        }
    }
}