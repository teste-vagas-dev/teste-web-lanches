using System.Threading.Tasks;
using TesteWebLanches.Services.Models;

namespace TesteWebLanches.Services
{
    public interface IOrderService
    {
        Task<OrderSummaryModel> CalculateOrder(IngredientsToCalculate ingredientsToCalculate);
    }
}