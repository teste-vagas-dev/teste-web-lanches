using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Swashbuckle.AspNetCore.Annotations;
using TesteWebLanches.Domain.Entities;
using TesteWebLanches.Services.Models;

namespace TesteWebLanches.Services
{
    public class RecipeService : IRecipeService
    {
        private readonly IRepository<Recipe> _recipeRepository;
        private readonly IIngredientService _ingredientService;

        public RecipeService(IRepository<Recipe> recipeRepository,
            IIngredientService ingredientService)
        {
            _recipeRepository = recipeRepository;
            _ingredientService = ingredientService;
        }
        
        // Get all recipes to order
        public async Task<List<RecipeToOrderModel>> GetAllRecipesToOrderAsync()
        {
            var recipes = await _recipeRepository.Query().AsNoTracking()
                .Include(p => p.RecipeIngredients)
                .ThenInclude(p => p.Ingredient)
                .ToListAsync();

            if (!recipes.Any())
            {
                return null;
            }

            var recipesToOrder = new List<RecipeToOrderModel>();
            foreach (var recipe in recipes)
            {
                var calculatedPrice = recipe.RecipeIngredients.Sum(p => p.Ingredient.Price * p.Quantity);
                var recipeModel = new RecipeToOrderModel
                {
                    Id = recipe.Id,
                    Name = recipe.Name,
                    IngredientsNumber = recipe.RecipeIngredients.Count,
                    Price = calculatedPrice
                };
                
                foreach (var recipeIngredient in recipe.RecipeIngredients)
                {
                    recipeModel.RecipeIngredientItems.Add(new IngredientItemModel
                    {
                        Id = recipeIngredient.Ingredient.Id,
                        Name = recipeIngredient.Ingredient.Name
                    });
                }
                
                recipesToOrder.Add(recipeModel);
            }
                
            return recipesToOrder;
        }

        // Create a custom recipe base in a existing one
        public async Task<IngredientRecipeModel> GetNewRecipeBaseByCreatedRecipeAsync(int recipeId)
        {
            var recipe = await _recipeRepository.Query()
                .Include(p => p.RecipeIngredients)
                .ThenInclude(p => p.Ingredient)
                .FirstOrDefaultAsync(p => p.Id == recipeId);
            
            if (recipe?.RecipeIngredients == null || !recipe.RecipeIngredients.Any())
            {
                return null;
            }

            var ingredients = recipe.RecipeIngredients.Select(recipeIngredient => new IngredientRecipeItemModel
            {
                IngredientId = recipeIngredient.IngredientId,
                Name = recipeIngredient.Ingredient.Name,
                Quantity = recipeIngredient.Quantity, 
                Price = recipeIngredient.Ingredient.Price
            }).ToList();

            var availableIngredients = await _ingredientService.GetAvailableIngredientsAsync();
            if (!availableIngredients.Any())
            {
                return null;
            }

            // Merge with available ingredients
            var newIngredientsToAdd = availableIngredients.Where(x => ingredients.All(y => y.IngredientId != x.Id));
            ingredients.AddRange(newIngredientsToAdd.Select(ingredient => new IngredientRecipeItemModel
            {
                IngredientId = ingredient.Id, 
                Name = ingredient.Name,
                Price = ingredient.Price,
                Quantity = 0
            }));

            var result = new IngredientRecipeModel
            {
                RecipeName = recipe.Name,
                IngredientRecipeItems = ingredients.OrderBy(p => p.Name).ToList()
            };

            return result;
        }
    }
}