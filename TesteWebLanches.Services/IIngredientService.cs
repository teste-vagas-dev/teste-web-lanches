using System.Collections.Generic;
using System.Threading.Tasks;
using TesteWebLanches.Services.Models;

namespace TesteWebLanches.Services
{
    public interface IIngredientService
    {
        Task<List<IngredientItemModel>> GetAvailableIngredientsAsync();
    }
}