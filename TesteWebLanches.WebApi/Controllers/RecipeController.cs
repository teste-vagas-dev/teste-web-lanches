using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using TesteWebLanches.Services;

namespace TesteWebLanches.WebApi.Controllers
{
    [Route("api/recipes")]
    [ApiController]
    public class RecipeController : ControllerBase
    {
        private readonly IRecipeService _recipeService;

        public RecipeController(IRecipeService recipeService)
        {
            this._recipeService = recipeService;
        }

        [HttpGet]
        public async Task<IActionResult> Get()
        {
            var recipes = await _recipeService.GetAllRecipesToOrderAsync();
            
            return Ok(recipes);
        }
        
        [HttpGet("get-new-recipe-by-created-recipe/{recipeId}")]
        public async Task<IActionResult> GetNewRecipeBasedByCreatedRecipe(int recipeId)
        {
            var recipe = await _recipeService.GetNewRecipeBaseByCreatedRecipeAsync(recipeId);
            
            return Ok(recipe);
        }
    }
}