using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using TesteWebLanches.Services;
using TesteWebLanches.Services.Models;
using IngredientItemModel = TesteWebLanches.WebApi.Models.IngredientItemModel;

namespace TesteWebLanches.WebApi.Controllers
{
    [Route("api/orders")]
    [ApiController]
    public class OrderController : Controller
    {
        private IOrderService _orderService;

        public OrderController(IOrderService orderService)
        {
            _orderService = orderService;
        }

        [HttpPost("calculate")]
        public async Task<IActionResult> Calculate(List<IngredientItemModel> ingredients)
        {
            if (ingredients == null || !ingredients.Any())
            {
                return BadRequest("Inform all ingredients, please ;)");
            }

            var request = new IngredientsToCalculate
            {
                Ingredients = ingredients.Select(ingredient => new IngredientRecipeItemModel
                {
                    IngredientId = ingredient.IngredientId,
                    Quantity = ingredient.Quantity
                }).ToList()
            };

            var response = await _orderService.CalculateOrder(request);

            return Ok(response);
        }
    }
}