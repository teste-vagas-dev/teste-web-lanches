using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using TesteWebLanches.Services;

namespace TesteWebLanches.WebApi.Controllers
{
    [Route("api/ingredients")]
    [ApiController]
    public class IngredientController : ControllerBase
    {
        private readonly IIngredientService _ingredientService;

        public IngredientController(IIngredientService ingredientService)
        {
            _ingredientService = ingredientService;
        }

        [HttpGet]
        public async Task<IActionResult> Get()
        {
            var result = await _ingredientService.GetAvailableIngredientsAsync();
            
            return Ok(result);
        }
    }
}