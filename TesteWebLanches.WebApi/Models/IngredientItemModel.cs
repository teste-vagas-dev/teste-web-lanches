namespace TesteWebLanches.WebApi.Models
{
    public class IngredientItemModel
    {
        public int IngredientId { get; set; }
        public int Quantity { get; set; }
    }
}