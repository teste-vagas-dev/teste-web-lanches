﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TesteWebLanches.WebApi.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Ingredient",
                columns: table => new
                {
                    Id = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Name = table.Column<string>(type: "TEXT", maxLength: 200, nullable: false),
                    Price = table.Column<decimal>(type: "TEXT", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Ingredient", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Recipe",
                columns: table => new
                {
                    Id = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Name = table.Column<string>(type: "TEXT", maxLength: 200, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Recipe", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "RecipeIngredient",
                columns: table => new
                {
                    Id = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    IngredientId = table.Column<int>(type: "INTEGER", nullable: false),
                    RecipeId = table.Column<int>(type: "INTEGER", nullable: false),
                    Quantity = table.Column<int>(type: "INTEGER", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RecipeIngredient", x => x.Id);
                    table.ForeignKey(
                        name: "FK_RecipeIngredient_Ingredient_IngredientId",
                        column: x => x.IngredientId,
                        principalTable: "Ingredient",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_RecipeIngredient_Recipe_RecipeId",
                        column: x => x.RecipeId,
                        principalTable: "Recipe",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Ingredient",
                columns: new[] { "Id", "Name", "Price" },
                values: new object[] { 1, "Alface", 0.40m });

            migrationBuilder.InsertData(
                table: "Ingredient",
                columns: new[] { "Id", "Name", "Price" },
                values: new object[] { 2, "Bacon", 2.00m });

            migrationBuilder.InsertData(
                table: "Ingredient",
                columns: new[] { "Id", "Name", "Price" },
                values: new object[] { 3, "Hambúrguer de carne", 3.00m });

            migrationBuilder.InsertData(
                table: "Ingredient",
                columns: new[] { "Id", "Name", "Price" },
                values: new object[] { 4, "Ovo", 0.80m });

            migrationBuilder.InsertData(
                table: "Ingredient",
                columns: new[] { "Id", "Name", "Price" },
                values: new object[] { 5, "Queijo", 1.50m });

            migrationBuilder.InsertData(
                table: "Recipe",
                columns: new[] { "Id", "Name" },
                values: new object[] { 1, "X-Bacon" });

            migrationBuilder.InsertData(
                table: "Recipe",
                columns: new[] { "Id", "Name" },
                values: new object[] { 2, "X-Burger" });

            migrationBuilder.InsertData(
                table: "Recipe",
                columns: new[] { "Id", "Name" },
                values: new object[] { 3, "X-Egg" });

            migrationBuilder.InsertData(
                table: "Recipe",
                columns: new[] { "Id", "Name" },
                values: new object[] { 4, "X-Egg Bacon" });

            migrationBuilder.InsertData(
                table: "RecipeIngredient",
                columns: new[] { "Id", "IngredientId", "Quantity", "RecipeId" },
                values: new object[] { 1, 2, 1, 1 });

            migrationBuilder.InsertData(
                table: "RecipeIngredient",
                columns: new[] { "Id", "IngredientId", "Quantity", "RecipeId" },
                values: new object[] { 2, 3, 1, 1 });

            migrationBuilder.InsertData(
                table: "RecipeIngredient",
                columns: new[] { "Id", "IngredientId", "Quantity", "RecipeId" },
                values: new object[] { 3, 5, 1, 1 });

            migrationBuilder.InsertData(
                table: "RecipeIngredient",
                columns: new[] { "Id", "IngredientId", "Quantity", "RecipeId" },
                values: new object[] { 4, 3, 1, 2 });

            migrationBuilder.InsertData(
                table: "RecipeIngredient",
                columns: new[] { "Id", "IngredientId", "Quantity", "RecipeId" },
                values: new object[] { 5, 5, 1, 2 });

            migrationBuilder.InsertData(
                table: "RecipeIngredient",
                columns: new[] { "Id", "IngredientId", "Quantity", "RecipeId" },
                values: new object[] { 6, 4, 1, 3 });

            migrationBuilder.InsertData(
                table: "RecipeIngredient",
                columns: new[] { "Id", "IngredientId", "Quantity", "RecipeId" },
                values: new object[] { 7, 3, 1, 3 });

            migrationBuilder.InsertData(
                table: "RecipeIngredient",
                columns: new[] { "Id", "IngredientId", "Quantity", "RecipeId" },
                values: new object[] { 8, 5, 1, 3 });

            migrationBuilder.InsertData(
                table: "RecipeIngredient",
                columns: new[] { "Id", "IngredientId", "Quantity", "RecipeId" },
                values: new object[] { 9, 4, 1, 4 });

            migrationBuilder.InsertData(
                table: "RecipeIngredient",
                columns: new[] { "Id", "IngredientId", "Quantity", "RecipeId" },
                values: new object[] { 10, 2, 1, 4 });

            migrationBuilder.InsertData(
                table: "RecipeIngredient",
                columns: new[] { "Id", "IngredientId", "Quantity", "RecipeId" },
                values: new object[] { 11, 3, 1, 4 });

            migrationBuilder.InsertData(
                table: "RecipeIngredient",
                columns: new[] { "Id", "IngredientId", "Quantity", "RecipeId" },
                values: new object[] { 12, 5, 1, 4 });

            migrationBuilder.CreateIndex(
                name: "IX_RecipeIngredient_IngredientId",
                table: "RecipeIngredient",
                column: "IngredientId");

            migrationBuilder.CreateIndex(
                name: "IX_RecipeIngredient_RecipeId",
                table: "RecipeIngredient",
                column: "RecipeId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "RecipeIngredient");

            migrationBuilder.DropTable(
                name: "Ingredient");

            migrationBuilder.DropTable(
                name: "Recipe");
        }
    }
}
