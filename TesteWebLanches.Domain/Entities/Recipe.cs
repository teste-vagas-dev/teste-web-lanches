using System.Collections.Generic;

namespace TesteWebLanches.Domain.Entities
{
    public class Recipe : Entity
    {
        public string Name { get; set; }
        
        public virtual IList<RecipeIngredient> RecipeIngredients { get; set; }
    }
}