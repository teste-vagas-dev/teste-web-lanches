using System.Collections.Generic;

namespace TesteWebLanches.Domain.Entities
{
    public class Ingredient : Entity
    {
        public string Name { get; set; }
        
        public decimal Price { get; set; }

        public virtual IList<RecipeIngredient> RecipeIngredients { get; set; }
    }
}