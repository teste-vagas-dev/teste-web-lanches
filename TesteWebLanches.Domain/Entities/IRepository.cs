using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TesteWebLanches.Domain.Entities
{
    public interface IRepository<TEntity> where TEntity : Entity
    {
        IQueryable<TEntity> Query();
        
        void Insert(TEntity obj);

        void Update(TEntity obj);

        void Delete(int id);

        IList<TEntity> Select();

        TEntity Select(int id);

        Task SaveChangesAsync();
    }
}