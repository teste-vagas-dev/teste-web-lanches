using Microsoft.EntityFrameworkCore;
using TesteWebLanches.Data.Mapping;
using TesteWebLanches.Domain.Entities;

namespace TesteWebLanches.Data.Context
{
    public class StoreContext : DbContext
    {
        public StoreContext(DbContextOptions<StoreContext> options) : base(options)
        {
        }
        
        public DbSet<Ingredient> Ingredients { get; set; }
        public DbSet<Recipe> Recipes { get; set; }
        public DbSet<RecipeIngredient> RecipeIngredient { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            
            modelBuilder.Entity<Ingredient>(new IngredientMap().Configure);
            modelBuilder.Entity<Recipe>(new RecipeMap().Configure);
            modelBuilder.Entity<RecipeIngredient>(new RecipeIngredientMap().Configure);
        }
    }
}