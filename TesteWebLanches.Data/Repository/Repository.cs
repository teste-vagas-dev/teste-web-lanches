using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using TesteWebLanches.Data.Context;
using TesteWebLanches.Domain.Entities;

namespace TesteWebLanches.Data.Repository
{
    public class Repository<TEntity> : IRepository<TEntity> where TEntity : Entity
    {
        protected readonly StoreContext _storeContext;
        
        private DbSet<TEntity> _dbSet;
        private readonly DbFactory _dbFactory;
        
        protected DbSet<TEntity> DbSet => _dbSet ??= _dbFactory.DbContext.Set<TEntity>();

        public Repository(StoreContext storeContext, DbFactory dbFactory)
        {
            _storeContext = storeContext;
            _dbFactory = dbFactory;
        }
        
        public IQueryable<TEntity> Query()
        {
            return DbSet;
        }

        public void Insert(TEntity obj)
        {
            _storeContext.Set<TEntity>().Add(obj);
        }

        public void Update(TEntity obj)
        {
            _storeContext.Entry(obj).State = EntityState.Modified;
        }

        public void Delete(int id)
        {
            _storeContext.Set<TEntity>().Remove(Select(id));
        }
        
        public Task SaveChangesAsync()
        {
            return _storeContext.SaveChangesAsync();
        }

        public IList<TEntity> Select() =>
            _storeContext.Set<TEntity>().ToList();

        public TEntity Select(int id) =>
            _storeContext.Set<TEntity>().Find(id);

    }
}