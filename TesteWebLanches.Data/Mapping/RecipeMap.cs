using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using TesteWebLanches.Domain.Entities;

namespace TesteWebLanches.Data.Mapping
{
    public class RecipeMap : IEntityTypeConfiguration<Recipe>
    {
        public void Configure(EntityTypeBuilder<Recipe> builder)
        {
            builder.ToTable("Recipe");

            builder.HasKey(p => p.Id);

            builder.Property(p => p.Name)
                .HasMaxLength(200);

            builder.HasData(new List<Recipe>
            {
                new Recipe{ Id = 1, Name = "X-Bacon" },
                new Recipe{ Id = 2, Name = "X-Burger" },
                new Recipe{ Id = 3, Name = "X-Egg" },
                new Recipe{ Id = 4, Name = "X-Egg Bacon" }
            });
        }
    }
}