using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using TesteWebLanches.Domain.Entities;

namespace TesteWebLanches.Data.Mapping
{
    public class IngredientMap : IEntityTypeConfiguration<Ingredient>
    {
        public void Configure(EntityTypeBuilder<Ingredient> builder)
        {
            builder.ToTable("Ingredient");

            builder.HasKey(p => p.Id);

            builder.Property(p => p.Name)
                .IsRequired()
                .HasMaxLength(200);

            builder.Property(p => p.Price)
                .IsRequired();

            builder.HasData(new List<Ingredient>
            {
                new Ingredient { Id = 1, Name = "Alface", Price = 0.40M },
                new Ingredient { Id = 2, Name = "Bacon", Price = 2.00M },
                new Ingredient { Id = 3, Name = "Hambúrguer de carne", Price = 3.00M },
                new Ingredient { Id = 4, Name = "Ovo", Price = 0.80M },
                new Ingredient { Id = 5 ,Name = "Queijo", Price = 1.50M }
            });
        }
    }
}