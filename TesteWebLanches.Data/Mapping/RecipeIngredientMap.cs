using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using TesteWebLanches.Domain.Entities;

namespace TesteWebLanches.Data.Mapping
{
    public class RecipeIngredientMap : IEntityTypeConfiguration<RecipeIngredient>
    {
        public void Configure(EntityTypeBuilder<RecipeIngredient> builder)
        {
            builder.ToTable("RecipeIngredient");

            builder.HasKey(p => p.Id);

            builder.HasOne(p => p.Ingredient)
                .WithMany(p => p.RecipeIngredients)
                .HasForeignKey(p => p.IngredientId);

            builder.HasOne(p => p.Recipe)
                .WithMany(p => p.RecipeIngredients)
                .HasForeignKey(p => p.RecipeId);

            // X-Bacon
            builder.HasData(new List<RecipeIngredient>
            {
                new RecipeIngredient { Id = 1, RecipeId = 1, IngredientId = 2, Quantity = 1 },
                new RecipeIngredient { Id = 2, RecipeId = 1, IngredientId = 3, Quantity = 1 },
                new RecipeIngredient { Id = 3, RecipeId = 1, IngredientId = 5, Quantity = 1 },
            });
            
            // X-Burger
            builder.HasData(new List<RecipeIngredient>
            {
                new RecipeIngredient { Id = 4, RecipeId = 2, IngredientId = 3, Quantity = 1 },
                new RecipeIngredient { Id = 5, RecipeId = 2, IngredientId = 5, Quantity = 1 },
            });
            
            // X-Egg
            builder.HasData(new List<RecipeIngredient>
            {
                new RecipeIngredient { Id = 6, RecipeId = 3, IngredientId = 4, Quantity = 1 },
                new RecipeIngredient { Id = 7, RecipeId = 3, IngredientId = 3, Quantity = 1 },
                new RecipeIngredient { Id = 8, RecipeId = 3, IngredientId = 5, Quantity = 1 },
            });
            
            // X-Egg Bacon
            builder.HasData(new List<RecipeIngredient>
            {
                new RecipeIngredient { Id = 9, RecipeId = 4, IngredientId = 4, Quantity = 1 },
                new RecipeIngredient { Id = 10, RecipeId = 4, IngredientId = 2, Quantity = 1 },
                new RecipeIngredient { Id = 11, RecipeId = 4, IngredientId = 3, Quantity = 1 },
                new RecipeIngredient { Id = 12, RecipeId = 4, IngredientId = 5, Quantity = 1 },
            });
        }
    }
}